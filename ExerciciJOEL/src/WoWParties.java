import java.util.Scanner;

public class WoWParties {

	/**
	 * Aquesta funcio demana a l'usuari tres variables corresponents a la quantitat
	 * de jugadors de cada classe que hi ha a la party
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		while (casos > 0) {
			int tank = sc.nextInt();
			int dps = sc.nextInt();
			int supp = sc.nextInt();
			calcularParty(casos, tank, dps, supp);
			casos--;
		}
		sc.close();
	}

	/**
	 * La funcio calcula si hi ha una relacio entre les tres variables, donades unes
	 * condicions: Hi ha el doble de DPS que de Tancs. Hi ha la meitat de Suports
	 * que de Tancs. -> DPS = Tanc * 2 -> Suport = Tanc / 2
	 * 
	 * @param tank El nombre de Tancs
	 * @param dps  El nombre de DPS
	 * @param supp El nombre de Suports
	 * @return Retorna el resultat, si es una bona party, o si falten/sobren alguna
	 *         clase
	 */
	public static String calcularParty(int casos, int tank, int dps, int supp) {
		// TODO Auto-generated method stub
		String resultat = null;
		if (tank > 0 && dps > 0 && supp > 0 && ((dps == tank * 2) && (supp == tank / 2))) {
			resultat = "Aquesta es una bona party";
			System.out.println(resultat);
		} else if (tank > 0 && dps > 0 && supp > 0) {
			if (dps > tank * 2) {
				resultat = "Sobren DPS";
				System.out.println(resultat);
			} else if (dps < tank * 2) {
				resultat = "Falten DPS";
				System.out.println(resultat);
			} else if (supp > tank / 2) {
				resultat = "Sobren Supports";
				System.out.println(resultat);
			} else if (supp < tank / 2) {
				resultat = "Falten Supports";
				System.out.println(resultat);
			} else if (tank > dps / 2 || tank < supp * 2) {
				resultat = "Sobren Tancs";
				System.out.println(resultat);
			} else if (tank < dps / 2 || tank > supp * 2) {
				resultat = "Falten Tancs";
				System.out.println(resultat);
			}
		} else {
			resultat = "Has introduit nombres no valids";
			System.out.println(resultat);
		}
		return resultat;
	}
}
