import static org.junit.Assert.*;

import org.junit.Test;

public class TestWowParties {

	@Test
	public void testPublic() {
		assertEquals("Aquesta es una bona party", WoWParties.calcularParty(1, 2, 4, 1));
		assertEquals("Sobren DPS", WoWParties.calcularParty(1, 2, 5, 1));
		assertEquals("Falten DPS", WoWParties.calcularParty(1, 2, 3, 1));
		assertEquals("Has introduit nombres no valids", WoWParties.calcularParty(1, 0, 5, 23));
	}
	
	@Test
	public void testPrivat() {
		assertEquals("Has introduit nombres no valids", WoWParties.calcularParty(1, -95465431, 354684154, 2));
		assertEquals("Sobren DPS", WoWParties.calcularParty(1, 2, 5, 1));
		assertEquals("Falten DPS", WoWParties.calcularParty(1, 2, 3, 1));
		assertEquals("Has introduit nombres no valids", WoWParties.calcularParty(1, 0, 0, 0));
	}
}
